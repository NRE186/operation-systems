﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OS_Lab5_part2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void findBtn_Click(object sender, EventArgs e)
        {
            items.Items.Clear();
            DirectoryInfo dir1 = new DirectoryInfo(folder_name.Text);
            FileInfo[] imageFiles = dir1.GetFiles(file_name.Text, SearchOption.AllDirectories); 
            foreach (FileInfo f in imageFiles)
            {
                items.Items.Add("Имя файла: " + f.Name);
                items.Items.Add("       Расположение файла: " + f.DirectoryName);
            }
        }
    }
}
