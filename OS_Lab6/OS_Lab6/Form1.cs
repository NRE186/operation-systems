﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OS_Lab6
{
    public partial class Form1 : Form
    {
        public string file = "C:\\test\\data.txt";
        public string res = "";
        public Form1()
        {
            InitializeComponent();
        }
        private void Writer()
        {
            try
            {
                using (StreamWriter Writer = new StreamWriter(file))
                {
                    char ch;
                    Random rnd = new Random();
                    for (int i = 0; i < 10; i++)
                    {
                        ch = (char)rnd.Next('A', 'Z');
                        res += ch;
                        Writer.Write(ch);
                    }
                    Writer.Write("\r\n");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void Reader()
        {
            try
            {
                StreamReader Reader = new StreamReader(file);
                data.Text = "Создан файл: " + file + "\n";
                data.Text += "Писатель записал данные: " + res + "\n";
                data.BeginInvoke((MethodInvoker)(() => data.Text += "Читатель прочитал данные: " + Reader.ReadToEnd()));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Application.Exit();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            CheckForIllegalCrossThreadCalls = false;
            Thread t_writer = new Thread(Writer);
            Thread t_reader = new Thread(Reader);
            t_writer.Start();
            t_reader.Start();
        }
    }
}
