﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OS_Lab3
{
    public partial class Form1 : Form
    {
        int n_factor;
        int result_factor = 1;
        public Form1()
        {
            InitializeComponent();
        }
        static int NOD(int x, int y)
        {
            while (x != y)
            {
                if (x > y)
                    x = x - y;
                else
                    y = y - x;
            }
            return x;
        }
        void Factorial()
        {
            n_factor = int.Parse(factor.Text);
            for (int i = 1; i <= n_factor; i++)
                result_factor *= i;
            int res = NOD(result_factor, n_factor);
            MessageBox.Show("Факториал = " + result_factor.ToString() + "; Нод = " + res);
        }
        private void solve_Click(object sender, EventArgs e)
        {
            Thread thread = new Thread(Factorial);
            thread.Start();
        }
    }
}
