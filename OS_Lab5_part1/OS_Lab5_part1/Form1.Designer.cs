﻿namespace OS_Lab5_part1
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.directory_name = new System.Windows.Forms.Label();
            this.get_directories = new System.Windows.Forms.Button();
            this.back = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.directories = new System.Windows.Forms.ListBox();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // directory_name
            // 
            this.directory_name.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.directory_name.AutoSize = true;
            this.directory_name.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.directory_name.Location = new System.Drawing.Point(339, 9);
            this.directory_name.Name = "directory_name";
            this.directory_name.Size = new System.Drawing.Size(147, 16);
            this.directory_name.TabIndex = 0;
            this.directory_name.Text = "Структура каталога: ";
            // 
            // get_directories
            // 
            this.get_directories.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.get_directories.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.get_directories.Location = new System.Drawing.Point(722, 3);
            this.get_directories.Name = "get_directories";
            this.get_directories.Size = new System.Drawing.Size(95, 23);
            this.get_directories.TabIndex = 1;
            this.get_directories.Text = "Отобразить";
            this.get_directories.UseVisualStyleBackColor = true;
            this.get_directories.Click += new System.EventHandler(this.get_directories_Click);
            // 
            // back
            // 
            this.back.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.back.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.back.Location = new System.Drawing.Point(0, 3);
            this.back.Name = "back";
            this.back.Size = new System.Drawing.Size(95, 23);
            this.back.TabIndex = 2;
            this.back.Text = "Назад";
            this.back.UseVisualStyleBackColor = true;
            this.back.Click += new System.EventHandler(this.back_Click);
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.get_directories);
            this.panel1.Controls.Add(this.back);
            this.panel1.Location = new System.Drawing.Point(12, 879);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(820, 31);
            this.panel1.TabIndex = 3;
            // 
            // directories
            // 
            this.directories.FormattingEnabled = true;
            this.directories.Location = new System.Drawing.Point(12, 31);
            this.directories.Name = "directories";
            this.directories.Size = new System.Drawing.Size(817, 836);
            this.directories.TabIndex = 4;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(844, 922);
            this.Controls.Add(this.directories);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.directory_name);
            this.Name = "Form1";
            this.Text = "Directories Viewer";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label directory_name;
        private System.Windows.Forms.Button get_directories;
        private System.Windows.Forms.Button back;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ListBox directories;
    }
}

