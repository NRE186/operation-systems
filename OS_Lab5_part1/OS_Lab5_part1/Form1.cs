﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OS_Lab5_part1
{
    public partial class Form1 : Form
    {
        DirectoryController DirController = new DirectoryController();

        private void FormUpdater()
        {
            directory_name.Text = DirController.Directory;
            directories.Items.Clear();
            foreach (string subDirectories in DirController.SubDirectories)
            {
                directories.Items.Add(subDirectories);
            }
        }

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            FormUpdater();
        }

        private void back_Click(object sender, EventArgs e)
        {
            DirController.GetPastDirectories();
            FormUpdater();
        }

        private void get_directories_Click(object sender, EventArgs e)
        {
            try
            {
                DirController.GetNextDirectories(directories.SelectedItem.ToString());
            }
            catch
            {
                MessageBox.Show("Произошла ошибка");
            }
            FormUpdater();
        }
    }
}
