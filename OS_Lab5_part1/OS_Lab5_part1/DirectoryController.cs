﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OS_Lab5_part1
{
    class DirectoryController
    {
        public String Directory;
        public String PastDirectory;
        public String[] SubDirectories;

        public DirectoryController()
        {
            Directory = "Выберите нужный диск";
            PastDirectory = "";
            SubDirectories = System.IO.Directory.GetLogicalDrives();
        }
        public void GetNextDirectories(string directory)
        {
            Directory = String.Join("Структура диска: ", directory);
            if(Directory.Length > 3)
            {
                PastDirectory = Directory.Substring(0, Directory.LastIndexOf('\\') + 1);
            }
            else
            {
                PastDirectory = "";
            }
            SubDirectories = System.IO.Directory.GetDirectories(directory);
        }
        public void GetPastDirectories()
        {
            if (PastDirectory != "")
            {
                Directory = String.Join("Структура диска: ", PastDirectory);
                SubDirectories = System.IO.Directory.GetDirectories(PastDirectory);
                if (Directory.Length > 3)
                {
                    PastDirectory = Directory.Substring(0, Directory.LastIndexOf('\\'));
                }
                else
                {
                    PastDirectory = "";
                }
            }
            else
            {
                Directory = "Выберите нужный диск";
                PastDirectory = "";
                SubDirectories = System.IO.Directory.GetLogicalDrives();
            }
        }
    }
}
