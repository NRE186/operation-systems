﻿using System;

namespace OS_Lab1
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.pc_name_label = new System.Windows.Forms.Label();
            this.pc_name = new System.Windows.Forms.Label();
            this.system_folders = new System.Windows.Forms.Label();
            this.system_folders_label = new System.Windows.Forms.Label();
            this.os_version_label = new System.Windows.Forms.Label();
            this.os_version = new System.Windows.Forms.Label();
            this.logical_drives_label = new System.Windows.Forms.Label();
            this.logical_drives = new System.Windows.Forms.Label();
            this.proccesors_count_label = new System.Windows.Forms.Label();
            this.proccesors_count = new System.Windows.Forms.Label();
            this.is_64bit_label = new System.Windows.Forms.Label();
            this.is_64bit_button = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // pc_name_label
            // 
            this.pc_name_label.AutoSize = true;
            this.pc_name_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.pc_name_label.Location = new System.Drawing.Point(13, 13);
            this.pc_name_label.Name = "pc_name_label";
            this.pc_name_label.Size = new System.Drawing.Size(188, 25);
            this.pc_name_label.TabIndex = 0;
            this.pc_name_label.Text = "Имя компьютера:";
            // 
            // pc_name
            // 
            this.pc_name.AutoSize = true;
            this.pc_name.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.pc_name.Location = new System.Drawing.Point(197, 13);
            this.pc_name.Name = "pc_name";
            this.pc_name.Size = new System.Drawing.Size(0, 25);
            this.pc_name.TabIndex = 1;
            // 
            // system_folders
            // 
            this.system_folders.AutoSize = true;
            this.system_folders.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.system_folders.Location = new System.Drawing.Point(317, 49);
            this.system_folders.Name = "system_folders";
            this.system_folders.Size = new System.Drawing.Size(0, 25);
            this.system_folders.TabIndex = 3;
            // 
            // system_folders_label
            // 
            this.system_folders_label.AutoSize = true;
            this.system_folders_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.system_folders_label.Location = new System.Drawing.Point(12, 49);
            this.system_folders_label.Name = "system_folders_label";
            this.system_folders_label.Size = new System.Drawing.Size(317, 25);
            this.system_folders_label.TabIndex = 2;
            this.system_folders_label.Text = "Путь к системным каталогам: ";
            // 
            // os_version_label
            // 
            this.os_version_label.AutoSize = true;
            this.os_version_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.os_version_label.Location = new System.Drawing.Point(13, 88);
            this.os_version_label.Name = "os_version_label";
            this.os_version_label.Size = new System.Drawing.Size(127, 25);
            this.os_version_label.TabIndex = 4;
            this.os_version_label.Text = "Версия ОС:";
            // 
            // os_version
            // 
            this.os_version.AutoSize = true;
            this.os_version.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.os_version.Location = new System.Drawing.Point(134, 88);
            this.os_version.Name = "os_version";
            this.os_version.Size = new System.Drawing.Size(0, 25);
            this.os_version.TabIndex = 5;
            // 
            // logical_drives_label
            // 
            this.logical_drives_label.AutoSize = true;
            this.logical_drives_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.logical_drives_label.Location = new System.Drawing.Point(13, 125);
            this.logical_drives_label.Name = "logical_drives_label";
            this.logical_drives_label.Size = new System.Drawing.Size(277, 25);
            this.logical_drives_label.TabIndex = 6;
            this.logical_drives_label.Text = "Имена логических дисков:";
            // 
            // logical_drives
            // 
            this.logical_drives.AutoSize = true;
            this.logical_drives.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.logical_drives.Location = new System.Drawing.Point(286, 125);
            this.logical_drives.Name = "logical_drives";
            this.logical_drives.Size = new System.Drawing.Size(0, 25);
            this.logical_drives.TabIndex = 7;
            // 
            // proccesors_count_label
            // 
            this.proccesors_count_label.AutoSize = true;
            this.proccesors_count_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.proccesors_count_label.Location = new System.Drawing.Point(13, 163);
            this.proccesors_count_label.Name = "proccesors_count_label";
            this.proccesors_count_label.Size = new System.Drawing.Size(312, 25);
            this.proccesors_count_label.TabIndex = 8;
            this.proccesors_count_label.Text = "Количество ядер процессора:";
            // 
            // proccesors_count
            // 
            this.proccesors_count.AutoSize = true;
            this.proccesors_count.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.proccesors_count.Location = new System.Drawing.Point(322, 163);
            this.proccesors_count.Name = "proccesors_count";
            this.proccesors_count.Size = new System.Drawing.Size(0, 25);
            this.proccesors_count.TabIndex = 9;
            // 
            // is_64bit_label
            // 
            this.is_64bit_label.AutoSize = true;
            this.is_64bit_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.is_64bit_label.Location = new System.Drawing.Point(17, 204);
            this.is_64bit_label.Name = "is_64bit_label";
            this.is_64bit_label.Size = new System.Drawing.Size(320, 25);
            this.is_64bit_label.TabIndex = 10;
            this.is_64bit_label.Text = "Является ли ОС 64 разрядной:";
            // 
            // is_64bit_button
            // 
            this.is_64bit_button.Location = new System.Drawing.Point(343, 204);
            this.is_64bit_button.Name = "is_64bit_button";
            this.is_64bit_button.Size = new System.Drawing.Size(159, 23);
            this.is_64bit_button.TabIndex = 11;
            this.is_64bit_button.Text = "Получить информацию";
            this.is_64bit_button.UseVisualStyleBackColor = true;
            this.is_64bit_button.Click += new System.EventHandler(this.is_64bit_button_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.ClientSize = new System.Drawing.Size(800, 800);
            this.Controls.Add(this.is_64bit_button);
            this.Controls.Add(this.is_64bit_label);
            this.Controls.Add(this.proccesors_count);
            this.Controls.Add(this.proccesors_count_label);
            this.Controls.Add(this.logical_drives);
            this.Controls.Add(this.logical_drives_label);
            this.Controls.Add(this.os_version);
            this.Controls.Add(this.os_version_label);
            this.Controls.Add(this.system_folders);
            this.Controls.Add(this.system_folders_label);
            this.Controls.Add(this.pc_name);
            this.Controls.Add(this.pc_name_label);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Environment Viewer";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label pc_name_label;
        private System.Windows.Forms.Label pc_name;
        private System.Windows.Forms.Label system_folders;
        private System.Windows.Forms.Label system_folders_label;
        private System.Windows.Forms.Label os_version_label;
        private System.Windows.Forms.Label os_version;
        private System.Windows.Forms.Label logical_drives_label;
        private System.Windows.Forms.Label logical_drives;
        private System.Windows.Forms.Label proccesors_count_label;
        private System.Windows.Forms.Label proccesors_count;
        private System.Windows.Forms.Label is_64bit_label;
        private System.Windows.Forms.Button is_64bit_button;
    }
}

