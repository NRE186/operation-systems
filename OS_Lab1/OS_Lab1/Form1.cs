﻿using System;
using System.Windows.Forms;

namespace OS_Lab1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            pc_name.Text = Environment.MachineName;
            system_folders.Text = Environment.SystemDirectory;
            os_version.Text = Environment.OSVersion.ToString();
            String[] drives = Environment.GetLogicalDrives();
            logical_drives.Text = String.Join("; ", drives);
            proccesors_count.Text = Environment.ProcessorCount.ToString();
        }

        private void is_64bit_button_Click(object sender, EventArgs e)
        {
            if (Environment.Is64BitOperatingSystem)
                MessageBox.Show(Text = "ОС является 64-х разрядной", Name="Информация о разрядности");
            else
                MessageBox.Show(Text = "ОС является 32-х разрядной", Name = "Информация о разрядности");
        }
    }
}
