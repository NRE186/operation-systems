﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OS_Lab4
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        List<string> Problem = new List<string>() { "Если вы видите это сообщение, то произошла ошибка" };
        string[] CommonResource = { "", "" };
        static Mutex mutexObj = new Mutex(false);

        List<string> MutexFunction(int i)
        {
            if (mutexObj.WaitOne(TimeSpan.FromSeconds(5), false))
            {
                if (i == 0)
                {
                    string text = textBox1.Text;
                    textBox1.Text = "";
                    CommonResource[0] = text;
                    List<string> Information = new List<string>() { };
                    if (CommonResource[0] != "")
                    {
                        Information.Add(CommonResource[0]);
                    }
                    CommonResource[0] = "";
                    mutexObj.ReleaseMutex();
                    return Information;
                }
                else
                {
                    string text = textBox2.Text;
                    textBox2.Text = "";
                    CommonResource[1] = text;
                    List<string> Information = new List<string>() { };
                    if (CommonResource[1] != "")
                    {
                        Information.Add(CommonResource[1]);
                    }
                    CommonResource[1] = "";
                    mutexObj.ReleaseMutex();
                    return Information;
                }
            }
            mutexObj.ReleaseMutex();
            return Problem;
        }

        void Reader1()
        {
            int i = 1;
            List<string> text = MutexFunction(i);
            foreach (string line in text)
            {
                listBox1.Items.Add(line);
            }
        }

        void Reader2()
        {
            int i = 0;
            List<string> text = MutexFunction(i);
            foreach (string line in text)
            {
                listBox2.Items.Add(line);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            CheckForIllegalCrossThreadCalls = false;
            Thread thread1 = new Thread(Reader1);
            Thread thread2 = new Thread(Reader2);
            thread1.Start();
            thread2.Start();
        }
    }
}
