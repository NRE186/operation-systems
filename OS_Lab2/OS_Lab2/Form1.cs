﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OS_Lab2
{
    public partial class Form1 : Form
    {
        public string directory = "C:\\Users\\12\\Downloads";
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            ShowListBoxFiles();
        }

        private void ShowListBoxFiles()
        {
            listBox1.Items.Clear();
            label2.Text = directory;
            DirectoryInfo dir1 = new DirectoryInfo(directory);
            FileInfo[] imageFiles = dir1.GetFiles("*.txt", SearchOption.AllDirectories);
            foreach (FileInfo f in imageFiles)
            {
                listBox1.Items.Add(f.Name);
            }
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            int fileIndex = listBox1.SelectedIndex;
            string filename = listBox1.Items[fileIndex].ToString();
            label1.Text = "Выбранный файл " + directory + "\\" + filename;
            using(StreamReader reader = new StreamReader(directory + "\\" + filename))
            {
                richTextBox1.Text = reader.ReadToEnd();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            using (var fbd = new FolderBrowserDialog())
            {
                DialogResult result = fbd.ShowDialog();

                if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                {
                    directory = fbd.SelectedPath;
                    label2.Text = directory;
                    ShowListBoxFiles();
                }
            }
        }
    }
}
